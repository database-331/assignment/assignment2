CREATE OR REPLACE PACKAGE book_store AS
    FUNCTION get_price_after_tax( book_isbn NUMBER)
    RETURN NUMBER;
    FUNCTION book_purchasers (book_isbn NUMBER)
    RETURN VARRAY;

END book_store;

/

CREATE OR REPLACE PACKAGE BODY book_store AS

        afterDiscount NUMBER;
        originalCost NUMBER;
        discountGiven NUMBER;
        afterTax Number;
        
    FUNCTION price_after_discount(book_isbn NUMBER)
    RETURN NUMBER
    AS
    originalCost books.cost%type;
    discountGiven books.discount%type;
    afterDiscount NUMBER;
    
    BEGIN
    
       SELECT
        b.retail INTO originalCost
        FROM
        Books b
        WHERE
        book_isbn=b.ISBN;
        

        SELECT
        NVL(b.discount,0) INTO discountGiven
        FROM
        Books b
        WHERE
        book_isbn=b.ISBN;
        
        afterDiscount:=originalCost-discountGiven;
       
        RETURN afterDiscount;
    END;
    
    FUNCTION get_price_after_tax(book_isbn NUMBER)
    RETURN NUMBER
    AS
    afterTax NUMBER;
    afterDiscount NUMBER;
    
        BEGIN
        afterDiscount:=book_store.price_after_discount(book_isbn);
            afterTax:= afterDiscount*1.15;
            RETURN afterTax;
            END;
            
    FUNCTION book_purchases(book_isbn NUMBER)
    RETURN VARRAY
    AS
    custID
        
END book_store;
/

DECLARE
book1 NUMBER;
book2 NUMBER;
b1Final NUMBER;
b2Final NUMBER;

BEGIN
    SELECT
    ISBN INTO book1
    FROM
    BOOKS
    WHERE
    title ='BUILDING A CAR WITH TOOTHPICKS';

    SELECT
    ISBN INTO book2
    FROM
    BOOKS
    WHERE
    title ='HOLY GRAIL OF ORACLE';
    
    b1Final:=book_store.get_price_after_tax(book1);
    b2Final:=book_store.get_price_after_tax(book2);
    dbms_output.put_line('BUILDING A CAR WITH TOOTHPICKS ' || b1Final);
    dbms_output.put_line('HOLY GRAIL OF ORACLE ' || b2Final);

END;
--/
--SELECT
--        b.cost 
--        FROM
--        Books b
--        WHERE
--        1059831198=b.ISBN;
--/
--SELECT
--        NVL(b.discount,0) 
--        FROM
--        Books b
--        WHERE
--        1059831198=b.ISBN;

